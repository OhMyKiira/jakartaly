const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}"
  ],
  theme: {

    fontFamily : { 
       mono : ["Roboto Mono" , 'monospace']
    },

    extend: {
      colors : { 
        orange : { 
          DEFAULT : "#FFBA9D"
        }, 

        grey : { 
          DEFAULT : "#474747" , 
          dark : "#777777" ,
          light : "#F4F4F4" , 
          primary : "#DFDFDF"
        }, 

        erorrState : "#891900"
      }, 

      fontsSize : { 
        sm: '.75rem',
        base: '1rem',
        lg: '1.1875rem',
        xl: '1.5rem',
      }
    },
  },
  plugins: [],
}
