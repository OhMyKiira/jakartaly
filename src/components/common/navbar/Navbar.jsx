import tw from "twin.macro";
import { useState } from "react";
import { Link } from "react-router-dom";
import { Transition } from '@headlessui/react'
import Container from "../../util/container/Container"
import UseResponsive from "../../util/responsive/Responsive"

import Menu from '../../../assets/icons/hamburger.svg'

import HeadingOne from "../../util/typography/HeadingOne"
import TextBase from "../../util/typography/TextBase"

const Nav = tw.nav`w-full bg-orange md:p-4`
const Flex = tw.div`flex justify-between items-center`
const FlexBox = tw.div`flex items-center md:gap-8 lg:gap-24`
const SearchGroup = tw.div`flex relative items-center overflow-hidden bg-grey-light md:w-64 w-full h-10 px-5 mt-10 md:mt-0 rounded`
const ButtonSearch = tw.button`absolute bg-grey text-white px-4 py-3 right-0 rounded cursor-pointer`
const SearchInput = tw.input`bg-grey-light placeholder:text-grey focus:outline-none font-mono`
const ListItem = tw.ul`sm:flex hidden sm:gap-16 lg:gap-24 font-mono`

// mobile version 
const NavMobile = tw.div`bg-orange w-40 p-8 sm:hidden block`
const ListMobile = tw.ul`flex flex-col justify-center gap-24 items-end`
const ButtonMobile = tw.button`mx-auto`

export default function Navbar() {
    const [showNavbar, setShowNavbar] = useState(false)
    const { isOnlyMobile, isTablet, isOnlyTablet } = UseResponsive()

    return (
        <>
            <Nav>
                <Container>
                    <Flex>
                        <HeadingOne> jakartaly </HeadingOne>

                        <FlexBox>
                            <ListItem>
                                <li>Home</li>
                                <li>About</li>
                                <li>Blog</li>
                                <li>Contact Us</li>
                            </ListItem>

                            {isTablet && (
                                <SearchGroup>
                                    <ButtonSearch type="button"></ButtonSearch>
                                    <SearchInput placeholder="Search"></SearchInput>
                                </SearchGroup>
                            )}

                            {isOnlyMobile && (
                                <button onClick={() => setShowNavbar(!showNavbar)}>
                                    <img src={Menu} alt="" />
                                </button>
                            )}
                        </FlexBox>
                    </Flex>

                    {isOnlyTablet && (
                        <SearchGroup>
                            <ButtonSearch type="button"></ButtonSearch>
                            <SearchInput placeholder="Search"></SearchInput>
                        </SearchGroup>
                    )}
                </Container>

                {/* only mobile list navigation */}
                <Transition
                    show={showNavbar}
                    enter="transition ease-100 duration-100 transform"

                >{(ref) => (
                    <NavMobile>
                        <ListMobile>
                            <li>Home</li>
                            <li>About</li>
                            <li>Blog</li>
                            <li>Contact Us</li>
                        </ListMobile>
                    </NavMobile>
                )}

                </Transition>
            </Nav>
        </>
    )
}