import tw from "twin.macro"
import ListBlog from "../../../data/data"
import UseResponsive from "../../util/responsive/Responsive"

import HeadingOne from "../../util/typography/HeadingOne"
import TextBase from '../../util/typography/TextBase'
import TextSmall from '../../util/typography/TextSmall'

const Section = tw.section`flex flex-col gap-10 bg-grey-light  p-6 rounded-xl`
const ParagraphGroup = tw.div`grid gap-12`
const ParagraphItems = tw.div`space-y-12`
const ParagraphChilds = tw.div`space-y-6`
const Lines = tw.hr`bg-grey w-full`

export default function Sidebar() {

    return (
        <>
                <Section>
                    <HeadingOne> Explore More</HeadingOne>

                    <ParagraphGroup>
                        {ListBlog.map((blog , index) => 
                            <ParagraphItems> 
                                <ParagraphChilds> 
                                    <TextSmall>{blog.date}</TextSmall>
                                    <HeadingOne key={index}>{blog.title}</HeadingOne>
                                </ParagraphChilds>
                                <Lines/>
                            </ParagraphItems>
                        )}
                    </ParagraphGroup>
                </Section>
        </>
    )
}