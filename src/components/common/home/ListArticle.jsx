import { Link } from 'react-router-dom'
import tw from 'twin.macro'
import ListBlog from '../../../data/data'


import HeadingOne from '../../util/typography/HeadingOne'
import TextBase from '../../util/typography/TextBase'
import TextSmall from '../../util/typography/TextSmall'

const Section = tw.section`grid`


const Article = tw.div`mt-10`
const ArticleList = tw.ul`grid gap-14`
const ArticleItems = tw.li`grid gap-6`
const Lines = tw.hr``

export default function ListArticle() {
    return (
        <>
            <Section>
                <HeadingOne>Recent Posts</HeadingOne>

                <Article> 
                   <ArticleList> 
                   {ListBlog.map((blog , index) => 
                        <ArticleItems key={index}> 
                            <TextSmall>{blog.date}</TextSmall>
                            <Link to='/blogs'>
                                    <HeadingOne>{blog.title}</HeadingOne>
                            </Link>
                            <Lines/>
                        </ArticleItems>
                    )}
                   </ArticleList>
                </Article>
            </Section>
        </>
    )
}