import tw from "twin.macro";

import HeadingOne from "../../util/typography/HeadingOne"
import TextBase from '../../util/typography/TextBase'
import TextSmall from '../../util/typography/TextSmall'

const Section = tw.section`bg-grey-light p-4 rounded-lg`
const Flex = tw.div`flex items-center gap-2 `
const FlexRow = tw.div`flex flex-col`
const HeroImage = tw.img`w-1/2 rounded-xl`

export default function Hero() { 
    return( 
        <>
            <Section> 
               <Flex> 
                    <HeroImage src="https://images.unsplash.com/photo-1583895976629-5bb8f8a64ab5?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fGNhcnxlbnwwfHwwfG9yYW5nZXw%3D&auto=format&fit=crop&w=800&q=60"></HeroImage>

                    <FlexRow> 
                        <HeadingOne> Jakartaly - Welcome to my Blogs</HeadingOne>    
                    </FlexRow>
               </Flex>
            </Section>
        </>
    )
}