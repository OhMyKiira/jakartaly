import tw from "twin.macro"
import Container from "../../util/container/Container"

import HeadingOne from '../../util/typography/HeadingOne'
import TextBase from '../../util/typography/TextBase'
import TextSmall from '../../util/typography/TextSmall'


const FooterBox = tw.section`w-full bg-orange mt-8 p-4 font-mono`
const Flex = tw.div`flex flex-wrap justify-between items-center gap-y-8`
const SocialIcons = tw.span`rounded bg-white px-4 py-2`
const SocialList = tw.div`flex flex-wrap gap-8 `

export default function Footer() { 
    return( 
        <> 
            <FooterBox> 
                <Container> 
                   <Flex> 
                        <HeadingOne>Jakartaly</HeadingOne>

                        <SocialList> <SocialIcons> 
                            m
                        </SocialIcons>

                        <SocialIcons> 
                            m
                        </SocialIcons>

                        <SocialIcons> 
                            m
                        </SocialIcons>

                        <SocialIcons> 
                            m
                        </SocialIcons>

                        <SocialIcons> 
                            m
                        </SocialIcons></SocialList>
                   </Flex>
                </Container>
            </FooterBox>
        </>
    )
}