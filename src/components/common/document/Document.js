import MetaTags from "react-meta-tags"

export default function Metatag() { 
    return ( 
        <> 
            <MetaTags> 
                <title>The Jakartaly - Blog. Independent</title>
                <meta name="title" content="The Jakartaly - Blog. Independent"></meta>
                <meta name="description" content="Jakartaly - Site to read Indonesian blogs and natively by English Language"></meta>

                <meta property="og:type" content="website"></meta>
                <meta property="og:url" content="https://metatags.io/"></meta>
                <meta property="og:title" content="The Jakartaly - Blog. Independent"></meta>
                <meta property="og:description" content="Jakartaly - Site to read Indonesian blogs and natively by English Language"></meta>
                <meta property="og:image" content="https://metatags.io/assets/meta-tags-16a33a6a8531e519cc0936fbba0ad904e52d35f34a46c97a2c9f6f7dd7d336f2.png"></meta>

                <meta property="twitter:card" content="summary_large_image"></meta>
                <meta property="twitter:url" content="https://metatags.io/"></meta>
                <meta property="twitter:title" content="The Jakartaly - Blog. Independent"></meta>
                <meta property="twitter:description" content="Jakartaly - Site to read Indonesian blogs and natively by English Language"></meta>
                <meta property="twitter:image" content="https://metatags.io/assets meta-tags-16a33a6a8531e519cc0936fbba0ad904e52d35f34a46c97a2c9f6f7dd7d336f2.png"></meta>
            </MetaTags>
        </>
    )
}