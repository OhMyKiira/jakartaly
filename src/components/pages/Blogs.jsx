import tw from "twin.macro";
import ListBlog from "../../data/data";
import Footer from "../common/footer/Footer";
import Navbar from "../common/navbar/Navbar";
import Container from "../util/container/Container";

import HeadingOne from '../util/typography/HeadingOne'
import TextBase from '../util/typography/TextBase'
import TextSmall from '../util/typography/TextSmall'


const Post = { 
    id : 0 , 
    date : 'January 20 , 2022' , 
    text : `Trying to buy a car often seems like an impossible affair, especially if it is an industry that you normally don't pay attention to. This article provides you with the basic information necessary to make an educated purchase. By following these tips you will get the best price on the right car.


    When you are going to buy a car, you need to know ahead of time which features are essential for you in the car. Have a clear picture of what you want, so you can search for the right price as well as the right car for your needs.
    
    
    Do not allow yourself to buy the first car that you see, even if it seems like it would be perfect for you. There are so many vehicles out there for sale that buying the first one may cause you to lose out on making a much better deal on a car you may like a bit more.
        

    Look online, and then head to the dealership. You should only go to a dealership when you know what model and manufacturer you are interested in. Check online to find out more about all the cars you want to learn about, dealerships and brands, too.
    
    
    Take a look at owner reviews before selecting the car you want to buy. These can be found online, at many different websites. Owner reviews give you a good idea about how much others enjoy the car, and if they would recommend it to friends and family. They offer a different perspective than professional reviews, and they are very informative for anyone who is car shopping.
    
    
    Take an extended test drive. Don't just take it for a quick spin through the neighborhood by yourself. Instead, enlist everyone who will be regularly riding in the car to share their opinions. Ask the dealer for a full afternoon test drive so that you have the chance to take it on the freeway to check things like the pickup and the blind spots, and spend some time really feeling the comfort of the interior.
    
    
    If you are car shopping and want to test drive some different cars, make sure you bring your license and insurance card with you. Many dealers will want a photo copy of them before you drive. This is just to protect them in case someone steals or damages a car. If you do not have them with you, they might not let you test drive.
    
    
    Always take any used car you are thinking of buying to a mechanic that you trust. Do not take the dealers word that the car is in good condition. They might have only owned the car for a few days or bought it from an auction. They really have very little knowledge of the vehicle you are trying to buy.
    
    
    Be careful about giving up your personal information. Dealers run your credit as soon as they are able to. You have to realize that the process of checking your credit can ultimately damage your score, so be careful in this respect. Do not provide the dealer with your identification information until after you have agreed on terms.
    
    
    Whenever shopping for a used car, it would be wise to stick with certified pre-owned vehicles as this greatly reduces the likelihood that you'll end up with a car that is in poor condition. Just make sure that the certification if offered by the car manufacturer rather than the dealer.
    
    
    If you are absolutely in love with a certain color or interior finish, don't settle for less. Though the specific vehicle that you want might not be in stock at your local dealership, ask them if they can get it for you. Most dealerships have relationships with other dealerships and will trade vehicles back and forth. Keep in mind though, you might end up paying a little bit extra to cover some of the costs of the trade.
    
    
    Never agree to pay the asking price! There are few cars for sale out there that do not have some room for negotiation in the price. When you have researched the vehicle, inspected it and given it your approval, negotiate the final price based off your findings. Do not settle on a price until you are happy with your investment.
    
    
    Most of us are looking for the best possible deal when shopping for our next vehicle. One way you can get a great deal is by utilizing the time of the month to your advantage. At the first of each month, auto dealerships must pay for each of the cars they have on their showroom floor. So, at the end of each month, these dealers are more apt to negotiate so they can get rid of some of these vehicles. If you shop during the last week of the month, you can get the best deals!
    
    
    When you follow the tips that this article provides, you will be happier with the purchase you make. Enjoy your car or truck more by paying less! Keep each one of these tips in mind while making your purchases or even window shopping. That way, you will make a much better decision.`
}

const BlogPost = tw.article``
const BlogImage = tw.img`w-full h-96`
const TextArticle = tw.p`whitespace-pre-line`

export default function Blogs() { 
    return ( 
        <> 
            <Navbar/>

                <BlogPost> 
                   <Container> 
                        <BlogImage src="https://images.unsplash.com/photo-1583895976629-5bb8f8a64ab5?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fGNhcnxlbnwwfHwwfG9yYW5nZXw%3D&auto=format&fit=crop&w=800&q=60"></BlogImage>
                        <TextBase><TextArticle>{Post.text}</TextArticle></TextBase>
                   </Container>
                </BlogPost>
            <Footer/>
        </>
    )
}