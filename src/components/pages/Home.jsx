import tw from 'twin.macro'
import Footer from '../common/footer/Footer'
import Hero from '../common/home/Hero'
import ListArticle from '../common/home/ListArticle'
import Navbar from '../common/navbar/Navbar'
import Sidebar from '../common/sidebar/Sidebar'
import Container from '../util/container/Container'

const FlexRow = tw.div`flex flex-col gap-24`
const FlexColumn = tw.div`flex flex-wrap lg:flex-nowrap gap-12 mt-16`

export default function Home() { 
    return ( 
        <> 
            <Navbar/>
            <Container> 
                <FlexColumn> 
                    <Sidebar/>
                    <FlexRow>
                        <Hero/>
                        <ListArticle/>
                    </FlexRow>
                </FlexColumn>
            </Container>
            <Footer/>
        </>
    )
}