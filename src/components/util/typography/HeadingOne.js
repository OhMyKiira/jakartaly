import tw from "twin.macro"

const HeadingOne = tw.h1`text-grey text-xl font-mono font-normal`

export default HeadingOne