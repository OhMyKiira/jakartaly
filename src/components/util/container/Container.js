import tw from "twin.macro";

const Container = tw.div`max-w-screen-lg mx-auto lg:container p-4 lg:p-0`

export default Container