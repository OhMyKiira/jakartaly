import { Global, css } from "@emotion/react";
import { GlobalStyles as BaseStyle } from "twin.macro" ;  

const costumeStyles = css`
  @font-face {
    font-family: "Roboto Mono" ;
    font-style: normal ;
    font-weight: 400 ;
    src: url('https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@400&display=swap') ;
  }

  @font-face { 
    font-family : "Roboto Mono Light" ; 
    font-style : normal ;
    font-weight : 300 ; 
    src :  url('https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@300&display=swap') ; 
  }
`;


export default function GlobalStyles() { 
    return ( 
      <> 
        <BaseStyle/> 
        <Global styles={costumeStyles} />
      </>
    )
}