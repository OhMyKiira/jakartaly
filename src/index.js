import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import GlobalStyles from './style/GlobalStyle';
import { BrowserRouter as HistoryRouters } from 'react-router-dom';

import * as serviceWorkerRegistration from './services/serviceWorkerRegistration';

ReactDOM.render(
  <React.StrictMode>
      <HistoryRouters> 
        <GlobalStyles/>
        <App/>
      </HistoryRouters>
  </React.StrictMode>,
  document.getElementById('root')
);

// registration pwa 
serviceWorkerRegistration.register();
