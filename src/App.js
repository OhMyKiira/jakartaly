import Metatag from "./components/common/document/Document";
import Home from "./components/pages/Home";
import Blogs from "./components/pages/Blogs"
import { Route, Routes } from "react-router-dom";
import NotFound from "./components/pages/NotFound";

function App() {
  return (
    <> 
        <Metatag/>

        <Routes> 
          <Route exact path="/" element={<Home/>}></Route>
          <Route path="blogs" element={<Blogs/>}></Route>
          <Route path="*" element={<NotFound/>}></Route>
        </Routes>
    </>
  );
}

export default App;
